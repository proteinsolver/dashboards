# ProteinSolver Dashboards

## Kubernetes (using Knative)

To deploy the dashboards to a Kubernetes cluster, use `kubectl` to apply the `sudoku-service.yaml` and `design-service.yaml` config files. You need to have Knative installed on your cluster. 

```bash
kubectl apply -f sudoku/service.yaml
kubectl apply -f design/service.yaml
```

## Docker compose

```bash
docker-compose -f sudoku/docker-compose.yaml up -d --force-recreate --renew-anon-volumes

docker-compose -f design/docker-compose.yaml up -d --force-recreate --renew-anon-volumes

gpg --decrypt --batch --yes --passphrase "${PGP_PASSPHRASE}" --output zfdesign/.env zfdesign/.env.gpg
docker-compose -f zfdesign/docker-compose.yaml up -d --force-recreate --renew-anon-volumes
```
